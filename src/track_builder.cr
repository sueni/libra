module TrackBuilder
  extend self

  def call(track_path)
    Metaflac.new(Track.new track_path).tagged_track
  end
end
