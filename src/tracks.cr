require "./actualizeable"
require "./time_extension"

struct Tracks
  include Actualizeable

  getter items : Array(Track)

  def initialize(@base : Base)
    @items = actual_tracks
    @new_items = [] of Track
  end

  def keep
    with self yield
    @base.save self
  end

  def to_yaml(io)
    @items.to_yaml io
  end

  private def actual_tracks
    @base.empty? ? Array(Track).new : Array(Track).from_yaml @base.content
  end

  private def add(track : Track)
    @items << track
    @new_items << track
  end

  private def update(track : Track)
    index(track).try do |idx|
      old_track = @items[idx]
      track.last_played = old_track.last_played
      @items[idx] = track
    end
  end

  private def update!(track : Track)
    index(track).try { |idx| @items[idx] = track }
  end

  private def remove(track : Track)
    index(track).try { |idx| @items.delete_at idx }
  end

  private def index(track)
    @items.index { |t| t.path == track.path }
  end

  forward_missing_to @items
end
