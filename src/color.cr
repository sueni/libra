module Color
  extend self

  macro add_color(name, *codes)
    def {{name}}(str)
      wrap(str, {{codes}})
    end
  end

  add_color artist, 33
  add_color genre, 2
  add_color title, 36
  add_color summary, 4

  add_color add, 36
  add_color del, 31
  add_color err, 31
  add_color rescan, 33
  add_color warn, 93
  add_color inv, 7

  add_color ago_d, 2
  add_color ago_h, 30, 102
  add_color ago_m, 30, 103
  add_color ago_curr, 30, 101

  private def wrap(str, color_codes)
    "\e[#{color_codes.join(';')}m#{str}\e[0m"
  end
end
