class Track
  include YAML::Serializable
  property path : String
  property album = ""
  property artist = ""
  property genre = ""
  property title = ""
  property last_played = Global::NOW
  property duration = 0

  def initialize(@path)
  end

  def joined_records
    {album.downcase,
     artist.downcase,
     genre.downcase,
     title.downcase}.join
  end

  def display(indent = Global::INDENT)
    span = Global::NOW - last_played

    puts String.interpolation(
      Ago.human_colored(span, indent, current?), ' ',
      Color.artist(artist), ' ',
      Color.title(title), ' ',
      album, ' ',
      Color.genre(genre.gsub(/ ?[|\/] ?/, ", "))
    )
  end

  private def current?
    path == Query.current_track
  end
end
