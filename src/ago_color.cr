struct AgoColor
  def initialize(@is_current : Bool)
  end

  def call(default_color)
    @is_current ? ->Color.ago_curr(String) : default_color
  end
end
