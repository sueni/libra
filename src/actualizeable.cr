module Actualizeable
  def actualize
    delete_obsolete_tracks
    add_untracked_tracks
    rescan_fresh_flacs
  end

  private def delete_obsolete_tracks
    obsolete = @items.select do |t|
      !existing_flacs.includes? t.path
    end
    @items -= obsolete
    obsolete.each { |t| puts Color.del t.path }
  end

  private def add_untracked_tracks
    untracked_flacs.shuffle!.each do |flac|
      puts Color.add(flac)
      add TrackBuilder.call flac
    end
  end

  private def rescan_fresh_flacs
    fresh_but_not_added_flacs.each do |flac|
      update(TrackBuilder.call flac)
      puts Color.rescan(flac)
    end
  end

  private def fresh_but_not_added_flacs
    fresh_flacs.reject do |ff|
      @new_items.find { |t| t.path == ff }
    end
  end

  private def fresh_flacs(span = 10.minutes)
    existing_flacs.select do |ef|
      (Global::NOW - File.info(ef).modification_time) < span
    end
  end

  private def untracked_flacs
    existing_flacs.reject do |ef|
      @items.find { |t| t.path == ef }
    end
  end

  private def existing_flacs
    @@flacs ||= Dir.glob("#{LIBRARY}/*flac")
  end
end
