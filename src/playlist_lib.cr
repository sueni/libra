require "./playlist"

struct PlaylistLib < Playlist
  def initialize(@tracks : Tracks, @quantity = Global::PLAYPACK)
  end

  def trackfiles
    return Filter.loose_filter(@tracks).map &.path if Filter.enabled?
    TrackSorter.new(@tracks).oldest_played(@quantity).map &.path
  end
end
