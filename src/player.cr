class Player
  def initialize(@playlist : Playlist)
  end

  def play
    return if @playlist.trackfiles.empty?
    with_hidden_cursor { system("mpvw", arguments) }
    @playlist.goodbye
  end

  private def player_args
    %w[-Top --profile=local-audio --shuffle]
  end

  private def arguments
    player_args.concat @playlist.trackfiles
  end

  private def with_hidden_cursor
    at_exit { print "\e[?25h" }
    Signal::INT.trap { exit }
    print "\e[?25l"
    yield
  end
end
