struct Metaflac
  CMD = "metaflac"
  @tags : Array(String)
  @duration_args : Tuple(String, String, String)

  def initialize(@track : Track)
    @tags = tags
    @duration_args = {
      "--show-total-samples",
      "--show-sample-rate",
      @track.path,
    }
  end

  def tagged_track
    @track.tap do |t|
      t.album = compose "album"
      t.artist = compose "artist"
      t.title = compose "title"
      t.genre = compose "genre"
      t.duration = duration
    end
  end

  private def compose(tag)
    @tags.select(&.downcase.starts_with?("#{tag}="))
      .map { |tag_line| tag_line.split('=', 2).last }
      .join(", ")
  end

  private def tags
    Process.run(
      CMD,
      {"--export-tags-to=-", @track.path},
      &.output.gets_to_end.split("\n")
    )
  end

  private def duration
    Process.run(CMD, @duration_args) do |meta|
      meta.output
        .gets_to_end
        .split("\n")
        .try { |(total, rate)| total.to_i // rate.to_i }
    end
  end
end
