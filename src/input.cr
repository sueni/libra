struct Input
  getter files = [] of InputFile

  def initialize(@argv : Array(String))
    parse
  end

  def files_from_lib
    @files.select &.flac_from_lib?
  end

  def files_not_from_lib
    @files.reject &.flac_from_lib?
  end

  def flacs
    @files.select &.flac?
  end

  private def parse
    until @argv.empty?
      if File.exists?(arg = @argv.shift)
        @files << InputFile.new arg
      else
        Filter.global << arg
      end
    end
  end
end
