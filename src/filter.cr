module Filter
  extend self
  class_property artist = [] of String
  class_property album = [] of String
  class_property title = [] of String
  class_property genre = [] of String
  class_property global = [] of String

  def disabled?
    !enabled?
  end

  def enabled?
    {artist,
     album,
     title,
     genre,
     global}.any? &.any?
  end

  def condense(*filters)
    filters.each { |filter| filter.reject!(&.size.< 3) }
  end

  def strict_filter(tracks : Tracks)
    return tracks.items if Filter.disabled?
    tracks.select { |t| passed_strict_tests(t).all? }
  end

  private def passed_strict_tests(t : Track)
    Array(Bool).new.tap do |tests|
      Filter.artist.any? && Filter.artist.any? do |f|
        tests << fsplit(t.artist.downcase).any? &.==(f)
      end
      Filter.album.any? && Filter.album.any? do |f|
        tests << fsplit(t.album.downcase).any? &.==(f)
      end
      Filter.title.any? && Filter.title.any? do |f|
        tests << fsplit(t.title.downcase).any? &.==(f)
      end
      Filter.genre.any? && Filter.genre.any? do |f|
        tests << fsplit(t.genre.downcase).any? &.==(f)
      end
      Filter.global.any? && Filter.global.any? do |f|
        tests << fsplit(t.joined_records).any? &.==(f)
      end
    end
  end

  def loose_filter(tracks : Tracks)
    return tracks.items if Filter.disabled?
    tracks.select { |t| passed_loose_tests(t).all? }
  end

  # FIXME: replace #includes? with #matches?
  private def passed_loose_tests(t : Track)
    Array(Bool).new.tap do |tests|
      Filter.artist.any? && Filter.artist.all? do |f|
        tests << t.artist.downcase.includes?(f)
      end
      Filter.album.any? && Filter.album.all? do |f|
        tests << t.album.downcase.includes?(f)
      end
      Filter.title.any? && Filter.title.all? do |f|
        tests << t.title.downcase.includes?(f)
      end
      Filter.genre.any? && Filter.genre.all? do |f|
        tests << t.genre.downcase.includes?(f)
      end
      Filter.global.any? && Filter.global.all? do |f|
        tests << t.joined_records.downcase.includes?(f)
      end
    end
  end

  private def fsplit(str)
    str.split(/\W+/)
  end
end
