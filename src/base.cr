require "compress/gzip"

struct Base
  def initialize(@path : String)
  end

  def content
    File.open(@path) do |base|
      Compress::Gzip::Reader.open base, &.gets_to_end
    end
  end

  def empty?
    !File.exists? @path
  end

  def save(tracks : Tracks)
    File.open(@path, "w") do |base|
      base.flock_exclusive do
        Compress::Gzip::Writer.open(
          base,
          Compress::Gzip::BEST_COMPRESSION
        ) { |gzip| tracks.to_yaml gzip }
      end
    end
  end
end
