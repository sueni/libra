struct Time
  def same_week?(other : Time)
    self.calendar_week.last == other.calendar_week.last
  end
end
