struct TrackSorter
  def initialize(@tracks : Tracks)
  end

  def oldest_played(quantity = Global::PLAYPACK)
    old_played.first(quantity)
  end

  def fresh_played
    old_played.reverse
  end

  private def old_played
    @tracks.sort_by(&.last_played)
  end
end
