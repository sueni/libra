abstract struct Playlist
  abstract def trackfiles

  def goodbye
    Process.new("aplay", {"--quiet", "#{__DIR__}/../media/beep.wav"}).wait
  end
end
