struct InputFile
  getter path : String

  def initialize(file)
    @path = File.expand_path file
  end

  def flac_from_lib?
    flac? && from_lib?
  end

  def from_lib?
    path.starts_with? LIBRARY
  end

  def flac?
    path.ends_with?(".flac")
  end
end
