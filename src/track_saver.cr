require "file_utils"

module TrackSaver
  extend self

  class_getter counter = 0

  def move(path, to library)
    "#{library}/#{File.basename path}".tap do |new_path|
      if File.exists?(new_path)
        puts "#{Color.warn("File exists:")} #{new_path}"
      else
        FileUtils.cp path, new_path
        FileUtils.rm path
        @@counter += 1
      end
    end
  end
end
