require "yaml"
require "./*"

LIBRARY = "/mnt/ssd1/music"
BASE    = "#{LIBRARY}/.base"

CLI.parse
input = Input.new(ARGV)

base = Base.new BASE
tracks = Tracks.new(base)
query = Query.new(tracks)

if input.files.empty?
  case CLI.action
  when Action::Actualize then tracks.keep { actualize }
  when Action::DirPlay   then Player.new(PlaylistDir.new).play
  when Action::Inform    then query.inform
  when Action::LibPlay   then Player.new(PlaylistLib.new tracks).play
  when Action::SeekPlay
    SeekPlayer.new(PlaylistLib.new tracks, tracks.size).play
  when Action::Query then query.search
  end
else
  case CLI.action
  when Action::Similar
    input.flacs.each { |infile| query.find_similar(infile) }
  when Action::Save
    input.files_not_from_lib.each do |infile|
      query.find_similar(infile)
    end
    input.files_not_from_lib.each do |infile|
      tracks.keep do
        TrackSaver.move(infile.path, to: LIBRARY)
          .try { |new_path| {add TrackBuilder.call new_path} }
      end
    end
    puts "Saved: #{TrackSaver.counter} Total: #{tracks.size}"
  end

  input.files_from_lib.each do |infile|
    case CLI.action
    when Action::Suggest
      query.suggested_track except: infile.path
    when Action::Update
      tracks.keep { update! TrackBuilder.call infile.path }
    when Action::Remove
      tracks.keep { remove TrackBuilder.call infile.path }
    end
  end
end
