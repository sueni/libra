require "./track_slice"

struct Query
  @@current_track : String?

  def self.current_track
    @@current_track ||= Process.run(
      "mpvc",
      {"--current-file"}
    ) { |proc| proc.output.gets(chomp: true) }
  end

  def initialize(@tracks : Tracks)
  end

  def inform(quantity = Global::PLAYPACK)
    fresh_tracks = TrackSorter.new(@tracks).fresh_played
    fresh_tracks.first(quantity).each &.display
    print "•" * Global::INDENT, ' ', @tracks.size, "\n"
    fresh_tracks.last(quantity).each &.display
  end

  def search
    abort Color.err("Empty query.") if Filter.disabled?
    slice = TrackSlice.new(Filter.loose_filter @tracks)
    slice.by_artist.each &.each(&.display)
  end

  def find_similar(file)
    track = TrackBuilder.call file.path

    Filter.artist.concat track.artist.split(/\W+/).map(&.downcase)
    Filter.title.concat track.title.split(/\W+/).map(&.downcase)
    Filter.condense(Filter.artist, Filter.title)

    TrackSlice.new(Filter.strict_filter @tracks).by_artist.each do |slice|
      puts Color.inv(File.basename file.path)
      slice.each(&.display)
    end
  end

  def suggested_track(except)
    puts(
      TrackSorter.new(@tracks)
        .oldest_played
        .reject { |t| t.path == except }
        .sample
        .path
    )
  end
end
