require "./playlist"

struct PlaylistDir < Playlist
  @pwd_glob : String

  def initialize
    abort("Can't run detached from TTY") unless STDOUT.tty?
    @pwd_glob = build_pwd_glob
    @subpwd_glob = "**/#{@pwd_glob}"
  end

  def trackfiles
    pwd_files.tap { |files| return files if files.any? }
    subpwd_files
  end

  private def build_pwd_glob
    %w[ape flac m4a mp3 ogg opus wv]
      .flat_map { |t| [t, t.upcase] }
      .join(',')
      .try { |j| "*.{#{j}}" }
  end

  private def pwd_files
    Dir.glob @pwd_glob
  end

  private def subpwd_files
    Dir.glob @subpwd_glob
  end
end
