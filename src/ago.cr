module Ago
  extend self

  def human_colored(span, indent, is_current = false)
    ago_color = AgoColor.new is_current
    pairs = ago_periods(span).zip(ago_colors ago_color)
    pairs.find { |(period, _)| period.active? }
      .try { |(period, color)| color.call(period.to_s.rjust(indent)) }
  end

  private def ago_periods(span)
    {
      AgoDays.new(span.days),
      AgoHours.new(span.hours),
      AgoMinutes.new(span.minutes),
      AgoSeconds.new(span.seconds),
    }
  end

  private def ago_colors(ago_color)
    {
      ago_color.call(->Color.ago_d(String)),
      ago_color.call(->Color.ago_h(String)),
      ago_color.call(->Color.ago_m(String)),
      ago_color.call(->Color.ago_m(String)),
    }
  end
end
