struct TrackSlice
  def initialize(@tracks : Array(Track))
  end

  def by_artist
    sliced_by_artist
      .map(&.sort_by!(&.last_played).reverse!)
  end

  private def sliced_by_artist
    @tracks
      .sort_by(&.artist)
      .slice_when { |x, y| x.artist != y.artist }
  end
end
