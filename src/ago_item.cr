abstract struct AgoItem
  getter value : Int32 | Int64

  def initialize(@value)
  end

  def to_s
    "#{value}#{suffix}"
  end

  def active?
    !@value.zero?
  end
end

struct AgoDays < AgoItem
  private def suffix
    'd'
  end
end

struct AgoHours < AgoItem
  private def suffix
    'h'
  end
end

struct AgoMinutes < AgoItem
  private def suffix
    'm'
  end
end

struct AgoSeconds < AgoItem
  private def suffix
    's'
  end
end
