require "option_parser"

module CLI
  extend self
  class_property action = Action::DirPlay

  @@commands : Array(String) = Action.names.map(&.downcase)
  @@help = "Valid commands are: #{@@commands.join(' ')}"

  def parse
    parse_options
    parse_command
  end

  def parse_command
    cmd = ARGV.shift? || return

    match = @@commands.select &.starts_with? cmd
    unless match.size == 1
      puts "Invalid command: #{cmd}"
      abort @@help
    end

    CLI.action = Action.parse match.last
  end

  def parse_options
    OptionParser.parse do |parser|
      parser.banner = "Usage: libra <command> [arguments]"
      parser.on("-h", "--help", "Show this help") do
        puts @@help
        puts parser
        exit
      end

      parser.on("-a ARTIST", "Filter by artist") do |s|
        set_query { Filter.artist.concat s.split(' ') }
      end
      parser.on("-b ALBUM", "Filter by album") do |s|
        set_query { Filter.album.concat s.split(' ') }
      end
      parser.on("-g GENRE", "Filter by genre") do |s|
        set_query { Filter.genre.concat s.split(' ') }
      end
      parser.on("-t TITLE", "Filter by title") do |s|
        set_query { Filter.title.concat s.split(' ') }
      end

      parser.invalid_option do |flag|
        STDERR.puts "ERROR: #{flag} is not a valid option."
        abort parser
      end
    end
  end

  private def set_query
    yield
    CLI.action = Action::Query
  end
end
