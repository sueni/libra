CRYSTAL ?= crystal
SRCDIR = $(CURDIR)/src
ALLSOURCES = $(shell find $(SRCDIR) -type f -name '*.cr')
PROJECT = $(shell basename $(CURDIR))
ALIAS = $(PROJECT)

ifeq ($(PROJECT), $(ALIAS))
OUTNAME = $(subst _,-,$(PROJECT))
else
OUTNAME = $(ALIAS)
endif

BINDIR = $(CURDIR)/bin
RELEASE_TARGET = $(BINDIR)/$(OUTNAME)
DEV_TARGET = $(MYTMPDIR)/$(OUTNAME)
#-------------------------------------------------------------------------------
$(RELEASE_TARGET): $(ALLSOURCES)
	make release
	make undev

.PHONY: release
release:
	make _prepbindir
	@$(CRYSTAL) build --no-color --release --no-debug $(SRCDIR)/$(PROJECT).cr -o $(RELEASE_TARGET)

.PHONY: _prepbindir
_prepbindir:
	@mkdir -p $(BINDIR)
	@echo /bin/ >> $(CURDIR)/.gitignore
	@sort --unique --output=$(CURDIR)/.gitignore $(CURDIR)/.gitignore

.PHONY: check
check:
	@$(CRYSTAL) run --no-color --no-codegen $(SRCDIR)/$(PROJECT).cr

.PHONY: dev
dev:
	@$(CRYSTAL) build --no-color $(SRCDIR)/$(PROJECT).cr -o $(DEV_TARGET)
	@ln --symbolic --force --target-directory=$(HOME)/.bin $(DEV_TARGET)

.PHONY: undev
undev:
	@ln --symbolic --force --target-directory=$(HOME)/.bin $(RELEASE_TARGET)
ifneq ("$(wildcard $(DEV_TARGET))","")
	@rm $(DEV_TARGET)
endif
